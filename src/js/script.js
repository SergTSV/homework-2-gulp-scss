"use strict";
const BURGER_BUTTON_CLASS_NAME = "burger-menu-button";
const ACTIVE_BURGER_BUTTON_CLASS_NAME = "burger-menu-button--active";
const ACTIVE_HEADER_MENU_CLASS_NAME = "menu--active";
const HEADER_MENU_CLASS_NAME = "menu";
const headerBurgerMenu = document.querySelector(`.${BURGER_BUTTON_CLASS_NAME}`);
const headerMenu = document.querySelector(`.${HEADER_MENU_CLASS_NAME}`);

headerBurgerMenu.addEventListener("click", ({ currentTarget: button }) => {
  button.classList.toggle(ACTIVE_BURGER_BUTTON_CLASS_NAME);
  button.classList.contains(ACTIVE_BURGER_BUTTON_CLASS_NAME)
    ? headerMenu.classList.add(ACTIVE_HEADER_MENU_CLASS_NAME)
    : headerMenu.classList.remove(ACTIVE_HEADER_MENU_CLASS_NAME);
});

document.addEventListener("click", ({ target }) => {
  if (
    !(
      target.closest(`.${BURGER_BUTTON_CLASS_NAME}`) ||
      target.closest(`.${HEADER_MENU_CLASS_NAME}`)
    )
  ) {
    headerBurgerMenu.classList.remove(ACTIVE_BURGER_BUTTON_CLASS_NAME);
    headerMenu.classList.remove(ACTIVE_HEADER_MENU_CLASS_NAME);
  }
});
